var rows=8;
var cols = 8;
var grid = [];
var score = 0;
var level = 1;
// Fruits used in the game, 8 types
var fruitsType=[];
var count = 0;

/**
 * This is the constructor of the Fruit-of-the-devil
 * @param image
 * @returns {{col: *, image: *, isInCombo: boolean, row: *, locked: boolean}}
 */
function fruit(image)
{
    return {
        image:image,
        locked:false,
        isInCombo:false
    }
}

/**
 * Lauchner of the game
 */
function startGame() {
    initFruitsType();
    prepareGrid();
    printGrid();
    var progressBar = document.getElementById("myBar");
    progressBar.style.background = "green";
    progressBar.style.width = "320px";
    progressBar.style.height = "20px";
    progressBar.style.margin = "auto";
    countDown(progressBar);
}

/**
 * Load table of fruits type
 */
function initFruitsType() {

    for (var i = 0; i<8;i++) {
        fruitsType[i]="../../public/img/devilFruit"+(i+1)+".jpg";
    }
}

/**
 *This methds return a random Fruit-of-the-Devil
 * @returns {*}
 */
function pickRandomFruit()
{
    return fruitsType[Math.floor((Math.random()*8))];
}

function prepareGrid() {

    for (var row = 0; row < rows; row++) {

        grid[row]=[];

        for (var col =0; col < cols; col++) {

            grid[row][col]=new fruit(pickRandomFruit());
        }
    }
    isThereACombination(true);
}

/**
 * Print the grid on the screen by creating new html elements
 */
function printGrid() {

    var div = document.getElementById('tableau');
    div.innerHTML = "";
    var tabEchange = [];

    for (var row = 0; row < rows; row++) {

        var tr = document.createElement('tr');
        for (var col = 0; col < cols; col++) {

            var cell = document.createElement('img');
            cell.src = grid[row][col].image;
            cell.height = 50;
            cell.width = 50;
            cell.id=(row)+','+(col);
            cell.className="fruit";
            cell.onclick = function() {
                var elmt = this;
                var idelmt = this.getAttribute('id');
                var srcElmt = this.getAttribute('src');
               // console.log(srcElmt);
                var coord = recupCoordonnee(idelmt, srcElmt);
                if(tabEchange.length === 0){
                    tabEchange.push(coord);
                } else if(tabEchange.length === 1){
                    tabEchange.push(coord);
                    exchangeFruits(tabEchange[0], tabEchange[1]);
                    printGrid();
                    tabEchange = []
                }
            };
            tr.appendChild(cell);
        }
        div.appendChild(tr);
    }
}

/**
 *
 */
function printScore() {

    var scoreText = document.getElementById('score');
    scoreText.innerHTML = "score : "+score;
}

/**
 *
 */
function replay() {
    score=0;
    window.location.reload();
}

/**
 *
 * @param imgId
 * @param srcElmt
 * @returns {(number|*)[]}
 */
function recupCoordonnee(imgId,srcElmt) {
    var x = parseInt(imgId.split(',')[0]);
    var y = parseInt(imgId.split(',')[1]);
    return [x, y, srcElmt];
}

/**
 * exchange 2 fruits in the grid
 *
 * @param coordFruit1
 * @param coordFruit2
 */
function exchangeFruits(coordFruit1,coordFruit2){

   // printGridInConsole();
    var fruit1 = grid[coordFruit1[0]][coordFruit1[1]];
    var fruit2 = grid[coordFruit2[0]][coordFruit2[1]];

    var differenceX = coordFruit1[0]-coordFruit2[0];
    var differenceY = coordFruit1[1]-coordFruit2[1];

    if((coordFruit1[0] !== coordFruit2[0]) && coordFruit1[1] !== coordFruit2[1]){ return;}

    if((differenceX !== -1 && differenceX !==1) && (differenceY !== -1 && differenceY !== 1)){ return; }

    var temp = fruit1;
    grid[coordFruit1[0]][coordFruit1[1]] = fruit2;
    grid[coordFruit2[0]][coordFruit2[1]] = temp;


    var exchange = false;

    for (var row=0; row<rows; row++) {
        for (var col=0; col<cols; col++) {
            if(-1 !== testGenerationLevel(row, col).indexOf(1) || testGenerationLevel(row,col).indexOf(2) !== -1) {
                exchange=true;
            }
        }
    }
    if(!exchange){
            grid[coordFruit1[0]][coordFruit1[1]] = fruit1;
            grid[coordFruit2[0]][coordFruit2[1]] = fruit2;

    } else {
        isThereACombination(false);
    }
   // printGridInConsole();
}

/**
 * if fruits are align vertically or horizontally, return 1 or 2
 *
 * @param x
 * @param y
 * @returns {[]}
 */
function testGenerationLevel(x,y) {

    var tab = [];
    var currentFruit = grid[x][y];

    if(currentFruit === null){ return tab;}

    if(y>1){
        var previousY = grid[x][y-1];
        if(previousY === null){ return tab;}
        if(previousY.image===currentFruit.image){
            var prePreviousY = grid[x][y-2];
            if(prePreviousY === null){ return tab;}
            if(prePreviousY.image===currentFruit.image){
                tab.push(1);
                console.log("push 1");
            }
        }
    }

    if(x>1){
        var previousX = grid[x-1][y];
        if(previousX === null){ return tab;}
        if(previousX.image===currentFruit.image){
            var prePreviousX = grid[x-2][y];
            if(prePreviousX === null){ return tab;}
            if(prePreviousX.image===currentFruit.image){
                tab.push(2);
                console.log("push 2");
            }
        }
    }

    return tab;
}

/**
 * Search combinations with the attribut isinCombo
 */
function isThereACombination(init) {

    console.log("isThereACombination called");

    for (var row=0; row<rows; row++) {
        for (var col=0; col<cols; col++) {
            var test = testGenerationLevel(row, col);
            if (test.indexOf(1) !== -1) {
                grid[row][col].isInCombo = true;
                grid[row][col-1].isInCombo = true;
                grid[row][col-2].isInCombo = true;
                console.log("fruit in combo");
            }
            if (test.indexOf(2) !== -1) {
                grid[row][col].isInCombo = true;
                grid[row-1][col].isInCombo = true;
                grid[row-2][col].isInCombo = true;
                console.log("fruit in combo");
            }
        }
    }
    removeFruitsInCombi(init);
}

/**
 * If a fruit is in combo, become null + increase score
 */
function removeFruitsInCombi(init){

    var nbRemovedFruits = 0;

    for ( var row=0; row<rows; row++ ) {
        for ( var col=0; col<cols; col++ ) {
            if(grid[row][col] !== null && grid[row][col].isInCombo ){
                grid[row][col]=null;
                nbRemovedFruits++;
            }
        }
    }
    console.log(nbRemovedFruits);
    if (! init) {
        switch (nbRemovedFruits) {
            case 10:
                score += 2000 * level;
                addWidth(400);
                break;
            case 9:
                score += 1300 * level;
                addWidth(260);
                break;
            case 8:
                score += 1300 * level;
                addWidth(220);
                break;
            case 7:
                score += 400 * level;
                addWidth(80);
                break;
            case 6:
                score += 200 * level;
                addWidth(40);
                break;
            case 5:
                score += 1000 * level;
                addWidth(200);
                break;
            case 4:
                score += 300 * level;
                addWidth(60);
                break;
            case 3:
                score += 100 * level;
                addWidth(50);
                break;
        }
    }
    printScore();
    replaceFruits(init);
}

/**
 * For now it only replace removed fruits by random ones
 */
function replaceFruits(init){

    init = false;
    var replaceAgain = false;

    for (var row=0; row<rows ; row++) {
        for (var col=0 ; col<cols ; col++) {

            if ( grid[row][col] === null ) {
                var i = row;
                var fruitADescendre = null;
                while ( grid[i][col] === null &&  i>0 ) {
                    fruitADescendre = grid[i-1][col];
                    console.log("while");
                    i--;
                }

                //SI on a trouver un fruit, on le fait tomber
                if ( fruitADescendre !== null ) {
                    grid[row][col] = fruitADescendre;
                    grid[i][col] = null;
                }
                else {
                    grid[i][col] = new fruit(pickRandomFruit()); // Pas de fruits au dessus donc random
                }
                replaceAgain = true;
            }
        }
    }
    if ( replaceAgain ) isThereACombination(init);
}

/**
 * reduce the size of the life bar
 * @param bar
 */
function countDown(bar) {

    if (count === 0) {
        count = 1;
        var width = parseInt(bar.style.width.slice(0,-2));
        var id = setInterval(frame, 500);
        function frame() {
            if (width <= 0) {
                clearInterval(id);
                count = 0;
                console.log("c'est perdu");
                console.log("votre est score est de :"+score);
                return;
            } else {

                width = parseInt(bar.style.width.slice(0,-2));
                width -= level*10;
                bar.style.width = width + "px";
                if(width>=320 && width<=630){
                    bar.style.background="#37E209";
                } else if(width<320 && width>=160){
                    bar.style.background="#F7EC06";
                } else if(width<160 && width>=80){
                    bar.style.background="#F78906";
                } else if(width<80){
                    bar.style.background="#F70606";
                }
            }
        }
    }
}

function addWidth(size){
    var bar = document.getElementById("myBar");
    var taille = parseInt(bar.style.width.slice(0,-2));
    if(taille <640){
        if((size+taille)<=640){
            bar.style.width = (taille+size) + "px";
        } else {
            bar.style.width = "640px";
        }
    }
}